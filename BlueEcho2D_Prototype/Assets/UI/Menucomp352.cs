using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

[RequireComponent (typeof(AudioSource))]
public class Menucomp352 : MonoBehaviour {
    public GameObject pausepanel;
    public GameObject backgroundpanel;
    public GameObject levelpanel;
	public GameObject shellPanel;
	public GameObject musicpanel;
	public GameObject alllevelpanel;
	public GameObject conversation;
	public GameObject conversation1;
	public GameObject conversation2;
	public GameObject conversation3;
	public Slider voiceslider;
	public Slider musicslider;
	public AudioSource music;
    public GameObject gameUI;
	private bool paused = true; 


	void Start () {
        music.volume = 0.2f;

        SetPaused(true);

        backgroundpanel.gameObject.SetActive(true);
        pausepanel.gameObject.SetActive(false);
        levelpanel.gameObject.SetActive(false);
		musicpanel.gameObject.SetActive(false);
		alllevelpanel.gameObject.SetActive(false);
		music.ignoreListenerVolume = true;
        
	//	myLoadedAssetBundle = AssetBundle.LoadFromFile("Assets/AssetsBundles/scenes2")
		//	scenePaths = myLoadedAssetBundle.GetAllScenePaths();

		if (PlayerPrefs.HasKey("AudioVolume")) {
			AudioListener.volume = PlayerPrefs.GetFloat("AudioVolume");
		} else {
			
			AudioListener.volume = 1;
		
		}



	}


	void Update () {
		if (!paused && Input.GetKeyDown (KeyCode.Escape)) {
			SetPaused (true);
		}





	}



	private void SetPaused(bool p)
	{

		paused = p;
		pausepanel.gameObject.SetActive(paused);
		Time.timeScale = paused ? 0 : 1;
        backgroundpanel.gameObject.SetActive(false);
	}

	public void OnPressedPlay()
	{

		SetPaused(false);
		shellPanel.gameObject.SetActive (false);
        alllevelpanel.gameObject.SetActive(false);
        backgroundpanel.gameObject.SetActive(false);
        levelpanel.gameObject.SetActive(false);
        gameUI.gameObject.SetActive(true);
		conversation3.gameObject.SetActive(false);


    }


	public void OnPressedQuit()
	{
		Application.Quit ();  
	}
    
    public void OnPressReturn()
    {
        SetPaused(false);
        pausepanel.gameObject.SetActive(false);


	}

    public void OnPressRestart()
    {
        //Application.LoadLevel(0);
		int scene = SceneManager.GetActiveScene().buildIndex;
		SceneManager.LoadScene(scene, LoadSceneMode.Single);
		Time.timeScale = 1;
    }
	public void OnPressmed()
	{    SetPaused (false);
		//Application.LoadLevel(0);
		SceneManager.LoadScene("scene2",LoadSceneMode.Additive);
		//int scene = SceneManager.GetActiveScene().buildIndex;
		//SceneManager.LoadScene(scene3, LoadSceneMode.Single);
		shellPanel.gameObject.SetActive (false);
		alllevelpanel.gameObject.SetActive(false);
		backgroundpanel.gameObject.SetActive(false);
		gameUI.gameObject.SetActive (true);
	}
	public void OnPresshard()
	{    SetPaused (false);
		//Application.LoadLevel(0);
		SceneManager.LoadScene("scene3",LoadSceneMode.Additive);
		//int scene = SceneManager.GetActiveScene().buildIndex;
		//SceneManager.LoadScene(scene3, LoadSceneMode.Single);
		shellPanel.gameObject.SetActive (false);
		alllevelpanel.gameObject.SetActive(false);
		backgroundpanel.gameObject.SetActive(false);
		gameUI.gameObject.SetActive (true);
	}

	public void OnPressmusic(){
        backgroundpanel.gameObject.SetActive(false);
        pausepanel.gameObject.SetActive(false);
		shellPanel.gameObject.SetActive (false);
		musicpanel.gameObject.SetActive (true);
		voiceslider.value = AudioListener.volume; 
		musicslider.value = music.volume; 
	}

	public void OnPressalllevel(){

		shellPanel.gameObject.SetActive (false);
		alllevelpanel.gameObject.SetActive (true);
        levelpanel.gameObject.SetActive(false);

	}

    public void OnPresslevel()
	{   backgroundpanel.gameObject.SetActive(false);
        shellPanel.gameObject.SetActive(false);
        alllevelpanel.gameObject.SetActive(false);
        levelpanel.gameObject.SetActive(true);
    }

	public void OnPressstart()
	{

		levelpanel.gameObject.SetActive(false);
		conversation.gameObject.SetActive(true);
	}

	public void OnPressclick1()
	{
		
		conversation.gameObject.SetActive(false);
		conversation1.gameObject.SetActive(true);

	}

	public void OnPressclick2()
	{
		conversation1.gameObject.SetActive(false);
		conversation2.gameObject.SetActive(true);

	}

	public void OnPressclick3()
	{
		conversation2.gameObject.SetActive(false);
		conversation3.gameObject.SetActive(true);
	}

		


	public void Applymusic(){
		pausepanel.gameObject.SetActive (true);
		musicpanel.gameObject.SetActive (false);
		AudioListener.volume = voiceslider.value; 
		music.volume = musicslider.value;
		PlayerPrefs.SetFloat ("AudioVolume", AudioListener.volume);

	}


}