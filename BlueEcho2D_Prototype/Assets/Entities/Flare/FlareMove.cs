﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FlareMove : MonoBehaviour {
    public Vector3 mousePos;
    public float tempFlrChrg;
    public GameObject glow;

    public float speed = 5;
    public bool activated = false;

    //public int maxSize = 20;
    //public float growRate = 1f;

    // Use this for initialization
    void Start () {
        mousePos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        tempFlrChrg = worldCloudScript.flareCharge;
        //Debug.Log("tempflrchrg is:" + tempFlrChrg);
        worldCloudScript.flareCharge = 0f;
    }
	
	// Update is called once per frame
	void Update () {
        flareTravel();
    }

    public void flareTravel ()
    {
        Vector3 diff = mousePos - transform.position;
        diff.z = 0;
        if (diff.x >= 0.1 || diff.y >= 0.1 || diff.x <= -0.1 || diff.y <= -0.1)
        {
            diff.Normalize();
            transform.Translate(diff * Time.deltaTime * speed);
           
            //activated = true;
            //Debug.Log("flare finish move");
        }
        else if (activated == false)
        {
            mousePos.z = 0.0f;
            //GameObject objectInstance = Instantiate(glow, mousePos, Quaternion.identity);
            //objectInstance.transform.parent = gameObject.transform;
            transform.GetChild(0).gameObject.SetActive(true);
            activated = true;
        }
    }
}
