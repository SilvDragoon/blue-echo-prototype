﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class spawnGlow : MonoBehaviour {
    public static float maxSize;
	public int minSize = 1;
    public float growRate = 0.00000001f;
	public float timeSpawned;
	public float timeB4Shrink = 3000.0f;
    private bool tempLockout = false;

	//boolean stops growing once the flare wants to shrink
	public bool isShrinking = false;

    public GameObject glow;

    // Use this for initialization
    void Start () {
		timeSpawned = Time.time * 1000;

        //maxSize is set to the fuel charging bar's size
        //maxSize = transform.parent.GetComponent<spawnFlare>().flrCharge;
        maxSize = transform.parent.gameObject.GetComponent<FlareMove>().tempFlrChrg * 40;
        // if (!tempLockout)
        // {
       // Debug.Log("flarecharge b4 is:" + worldCloudScript.flareCharge);
        // worldCloudScript.flareCharge = 0.0f;
        //Debug.Log("flarecharge after is:" + worldCloudScript.flareCharge);
        //     tempLockout = true;
        // }
    }
	
	// Update is called once per frame
	void Update () {

        
        
        //Debug.Log("maxSize " + maxSize);

        //if (glow.activeInHierarchy == true) {
        if(glow.gameObject.activeSelf == true) {
            glowing();
        }
    }

    public void glowing()
    {
       

        //store scale of this transform in temporary variable
        Vector3 temp = transform.localScale;
		
		//timer variable
		float currenTime = Time.time*1000;
		float timeElapsed  = currenTime - timeSpawned;
		
        if ((transform.localScale.x < maxSize) && (!isShrinking) && !tempLockout)
        {
            //increase temp values
            //change the values for this saved variable (not actual transform scale)
            temp.x += growRate;
            temp.y += growRate;
        }

        else if((transform.localScale.x >= maxSize) || (isShrinking)){
            tempLockout = true;
			if ((timeElapsed >= timeB4Shrink) && (transform.localScale.x >= minSize)){
				
                //decrease temp values
				temp.x -= growRate;
				temp.y -= growRate;
				isShrinking = true;
			}

            //destroy the flare light after it shrinks completely
            if (transform.localScale.x <= minSize)
            {
                Destroy(transform.parent.gameObject);
                //destroy flare
                //Destroy(GameObject.Find("powerupYellow(Clone)"));
            }
		}
		
        //assign temp variable back to transform scale
        transform.localScale = temp;
    }
    
}
