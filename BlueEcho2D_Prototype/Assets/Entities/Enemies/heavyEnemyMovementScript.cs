﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class heavyEnemyMovementScript : MonoBehaviour
{
    public Vector3 target;
    public static int size = 2;

    public Vector3[] ArrayPoints = new Vector3[size];
    public int arrIndex;

    public float speed = 1f;


    public Vector3 Movement;
    // Use this for initialization
    void Start()
    {

        arrIndex = 0;

        target = ArrayPoints[arrIndex];


    }

    // Update is called once per frame
    void Update()
    {
        Movement = target - transform.position;
        Movement.z = 0f;

        if (Movement.x >= 0.1 || Movement.x <= -0.1 || Movement.y <= -0.1 || Movement.y >= 0.1)
        {
            Movement.Normalize();
            transform.Translate(new Vector3(Movement.x * speed, Movement.y * speed, 0) * Time.deltaTime);


        }
        else
        {
            //Debug.Log(size);
            if (arrIndex == (size - 1))
            {
                arrIndex = 0;
            }
            else
            {
                arrIndex++;
            }

            target = ArrayPoints[arrIndex];
        }
    }
}
