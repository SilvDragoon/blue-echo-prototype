﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyProjectileScript : MonoBehaviour
{
    public float damage = 100;
    public float speed = 3f;
     public Vector3 Movement;
    public Vector3 PlayerPos;

    public float GetDamage()
    {
        return damage;
    }

    public void Hit()
    {
        Destroy(this.gameObject);
    }

    private void OnTriggerEnter2D(Collider2D other)
    {	//Debug.Log("enemy attack collided with somehting " + other.gameObject);
		//Destroy(other.gameObject);
        if (other.gameObject.tag == "PlayerPhysical")
        {
            //Debug.Log("collision detected");
            Destroy(this.gameObject);


        }


    }

    /* void OnCollisionEnter(Collision col)
     {
             if(col.gameObject.tag == "BasicEnemy"){
                 Debug.Log("collision detected");
                 Destroy(this);

             }
             //Destroy(col.gameObject);

     }*/

    // Use this for initialization
    void Start()
    {	
		if(!GameObject.FindWithTag("PlayerPhysical")){
			Destroy(this.gameObject);
		}
		GameObject temp = GameObject.FindWithTag("PlayerPhysical");
        PlayerPos = temp.transform.position;
		//transform.InverseTransformPoint(GameObject.FindWithTag("PlayerPhysical").transform.position);
		//Debug.Log("player pos :"+ PlayerPos);

        //test rotation code
        Movement = PlayerPos - transform.position; 
        Movement.z = 0f;
        Movement.Normalize();


        float rot_z = Mathf.Atan2(Movement.y, Movement.x) * Mathf.Rad2Deg;
        transform.rotation = Quaternion.Euler(0f, 0f, rot_z - 90);

    }

    // Update is called once per frame
    void Update()
    {


        transform.Translate(new Vector3(0, speed, 0) * Time.deltaTime);


    }
}
