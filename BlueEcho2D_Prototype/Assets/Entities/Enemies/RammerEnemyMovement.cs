﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RammerEnemyMovement : MonoBehaviour {
    /*
    public int moveSpeed = 1;
    public Vector3 target;
    private bool playerAlive;
    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {

        Vector3 difference;
        //Debug.Log(moveSpeed);
        if (!GameObject.FindWithTag("PlayerPhysical"))
        {
            playerAlive = false;
            target = new Vector3(0, 0, 0);
        }
        else if (GameObject.FindWithTag("PlayerPhysical"))
        {
            playerAlive = true;
            target = GameObject.FindWithTag("PlayerPhysical").transform.position;
        }
        //float step = moveSpeed * Time.deltaTime;
        //transform.position = Vector3.MoveTowards(transform.position, target.position, step);

        //Debug.Log("target pos"+ target);
        difference = target - transform.position;
        //Debug.Log("difference pos" + difference);

        difference.Normalize();
        float rotation_z = Mathf.Atan2(difference.x, difference.y) * Mathf.Rad2Deg;
        transform.rotation = Quaternion.Euler(0, 0, -rotation_z);

       
        //transform.Translate(new Vector3(difference.x* moveSpeed, difference.y*moveSpeed, 0) * Time.deltaTime);
        transform.Translate(new Vector3(0, 1, 0) * Time.deltaTime);
    }
    */

    public float speed = 1.0f;
    public Transform target;

    void Update()
    {
        Vector2 direction = target.position - transform.position;
        direction = direction.normalized;

        Vector2 velocity = direction * speed;
        transform.Translate(velocity * Time.deltaTime);
    }
}
