using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class heavyEnemyRotationScript : MonoBehaviour {
	public float rotSpeed = 2.000f;
	private Quaternion lookRotation;
	public Vector3 target;
	private bool playerAlive;
	// Use this for initialization
	void Start () {
		
	
		 
		 
		
	}
	
	// Update is called once per frame
	/*void Update () {
		Vector3 difference;
		
		if (!GameObject.FindWithTag("PlayerPhysical")){
			playerAlive = false;
			target = new Vector3(0,0,0);
		}else if (GameObject.FindWithTag("PlayerPhysical")){
			playerAlive = true;
			target =GameObject.FindWithTag("PlayerPhysical").transform.position;
		}
	
			
		
		difference = target - transform.position;
		float step = rotSpeed * Time.deltaTime;
		Vector3 newDir = Vector3.RotateTowards (transform.forward, difference, step, 0.0f);
		Debug.DrawRay(transform.position, newDir, Color.red);
		transform.rotation = Quaternion.LookRotation(difference);
        //difference.Normalize();
        //float rotation_z = Mathf.Atan2(difference.x, difference.y) * Mathf.Rad2Deg;
        //transform.rotation = Quaternion.Euler(0, 0, -rotation_z);
		//transform.Rotate()
		
	}*/

	void Update () {
		Vector3 difference;

		if (!GameObject.FindWithTag("PlayerPhysical")){
			playerAlive = false;
			target = new Vector3(0,0,0);
		}else if (GameObject.FindWithTag("PlayerPhysical")){
			playerAlive = true;
			target =GameObject.FindWithTag("PlayerPhysical").transform.position;
		}

		difference = (target - transform.position);

		difference.Normalize();

		difference.z = 0f;
		//difference.y = 0f;
		//difference.x = 0f;

		lookRotation = Quaternion.LookRotation (difference);
		transform.rotation = Quaternion.Slerp (transform.rotation, lookRotation, Time.deltaTime * rotSpeed);



	}
}
