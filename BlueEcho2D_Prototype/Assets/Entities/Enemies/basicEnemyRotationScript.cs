﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class basicEnemyRotationScript : MonoBehaviour {
	public float rotSpeed = 0.004f;
	public Vector3 target;
	private bool playerAlive;
	// Use this for initialization
	void Start () {
		
	
		 
		 
		
	}
	
	// Update is called once per frame
	void Update () {
		Vector3 difference;
		
		if (!GameObject.FindWithTag("PlayerPhysical")){
			playerAlive = false;
			target = new Vector3(0,0,0);
		}else if (GameObject.FindWithTag("PlayerPhysical")){
			playerAlive = true;
			target =GameObject.FindWithTag("PlayerPhysical").transform.position;
		}
	
			
		
		difference = target - transform.position;
		
        difference.Normalize();
        float rotation_z = Mathf.Atan2(difference.x, difference.y) * Mathf.Rad2Deg;
        transform.rotation = Quaternion.Euler(0, 0, -rotation_z);
		
	}
}
