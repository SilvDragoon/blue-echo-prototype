﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyShootScript : MonoBehaviour {
	//public var minWait = 3.0;
	// public var maxWait = 5.0;
	 public GameObject objectToSpawn;
    public float timeSpawned;
	private int interval; 
	public int min = 1000;
	public int max = 4000;




    // Use this for initialization
    void Start () {
		timeSpawned = Time.time * 1000;
		interval = Random.Range(min, max);
    }
	
	// Update is called once per frame
	void Update () {

        float currenTime = Time.time * 1000;
        float timeElapsed = currenTime - timeSpawned;
        // yield WaitForSeconds(Random.Range(minWait, maxWait));
        if (timeElapsed >= interval)
        {
			
				
			//Debug.Log("created enemy shot");
             GameObject ObjectInstance = Instantiate(objectToSpawn, transform.position, Quaternion.Euler(new Vector3(0, 0, 0)));
            //Debug.Log(" after created enemy shot");
			
			timeSpawned = Time.time * 1000;
			interval = Random.Range(min,max);
        }
   }
}
