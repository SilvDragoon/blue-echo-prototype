﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemySpriteRenderer : MonoBehaviour
{
    public LayerMask lightLayer;
    public HashSet<Collider2D> lights;

    private SpriteRenderer spriteRenderer;
    public float EntityHp = 100f;
    public bool revealed = false;

    public AudioClip explosion;
    private AudioSource audio;

    //public Bounds enemyCollider;
    //public Bounds lightCollider;

    //public LayerMask lightLayer;

    void OnCollisionEnter2D(Collision2D col)
    {
        if (col.gameObject.name == "Circle")
        {
            this.spriteRenderer.enabled = true;
            revealed = true;

           // Debug.Log("sprite rendered");
        }
    }

    // Use this for initialization
    void Start()
    {
        this.spriteRenderer = GetComponent<SpriteRenderer>();
        this.spriteRenderer.enabled = false;

        lights = new HashSet<Collider2D>();

        //enemyCollider = GetComponent<Collider2D>().bounds;
        //lightCollider = GetComponent<Collider2D>().bounds;

        audio = GetComponent<AudioSource>();

        //lightLayer = 1 << 12;
        //lightLayer = ~lightLayer;
    }

    // Update is called once per frame
    void Update()
    {
        /*
        //Vector3 fwd = transform.TransformDirection(Vector3.forward);
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        if (Physics.Raycast(ray, 20, lightLayer))
        {
            spriteRenderer.enabled = false;
            Debug.Log("ray sensed");
        }
        */
       // Debug.Log(lights);
    }

    void OnTriggerEnter2D(Collider2D collider)
    {
        //Debug.Log("collided with" + collider.gameObject.tag);
        if (lightLayer.Contains(collider.gameObject))
        {
            lights.Add(collider);
            spriteRenderer.enabled = true;
            revealed = true;

            foreach(object item in lights)
            {
               // Debug.Log("add " + item);
            }
        }else  if (collider.gameObject.tag == "PlayerLaser")
        {
            //Debug.Log(EntityHp);
            //get the damage variable - hp
            if (revealed)
            {
                EntityHp -= collider.gameObject.GetComponent<ProjectileScript>().damage;

            }
            //Debug.Log(EntityHp);
            if (EntityHp <= 0f)
            {
                //plays sound as the enemy's location
                AudioSource.PlayClipAtPoint(explosion, this.transform.position);
                Destroy(transform.parent.gameObject);
                //Debug.Log("destroyed");
            }
        }
    }

    void OnTriggerExit2D(Collider2D collider)
    {
        if (lightLayer.Contains(collider.gameObject))
        {
            lights.Remove(collider);
            if (lights.Count == 0)
            {
                spriteRenderer.enabled = false;
                revealed = false;
            }
            foreach (object item in lights)
            {
               // Debug.Log("minus " + item);
            }
        }
    }
    /*
    private void OnTriggerEnter2D(Collider2D other)
    {

        if (other.gameObject.tag == "FlareZone")
        {
            //delay enabling sprite renderer or it will appear before flare light enlarges
            StartCoroutine(delay());
        }
        else if (other.gameObject.tag == "PlayerLaser")
        {
            Debug.Log(EntityHp);
            //get the damage variable - hp

            EntityHp -= other.gameObject.GetComponent<ProjectileScript>().damage;

            Debug.Log(EntityHp);
            if (EntityHp <= 0f)
            {
                //plays sound as the enemy's location
                AudioSource.PlayClipAtPoint(explosion, this.transform.position);
                Destroy(this.gameObject);
                Debug.Log("destropyed");
            }
        }
    }
    

    IEnumerator delay() {
        yield return new WaitForSeconds(0.2f);
        this.spriteRenderer.enabled = true;
    }
	
	private void OnTriggerExit2D(Collider2D other) {
		this.spriteRenderer.enabled = false;
	}
	*/
}
