﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShipMove : MonoBehaviour {
    public float speed = 2f;
    public float spacing = 5.0f;
    private Vector3 pos;

	// Use this for initialization
	void Start () {
        pos = transform.position;
	}
	
	// Update is called once per frame
	void Update () {
        if (Input.GetKey(KeyCode.W))
            pos.y += spacing;

        if (Input.GetKey(KeyCode.A))
            pos.x -= spacing;

        if (Input.GetKey(KeyCode.S))
            pos.y -= spacing;

        if (Input.GetKey(KeyCode.D))
            pos.x += spacing;

        transform.position = Vector3.MoveTowards(transform.position, pos, speed * Time.deltaTime);
    }
}
