﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class spawnPrimaryAttackscript : MonoBehaviour {
    public GameObject objectToSpawn;
	private float timeSpawned;
	private int shootWaitTime = 400;

    // Use this for initialization
    void Start () {
		timeSpawned = Time.time * 1000;
		
	}
	
	// Update is called once per frame
	void Update () {
		float currenTime = Time.time * 1000;
        float timeElapsed = currenTime - timeSpawned;
		
        if (Input.GetMouseButtonDown(0))
        {
            if(timeElapsed > shootWaitTime ){
            
				GameObject objectInstance = Instantiate(objectToSpawn, transform.position, Quaternion.Euler(new Vector3(0, 0, 0)));
				timeSpawned = Time.time * 1000;
			}
        }
    }
}
