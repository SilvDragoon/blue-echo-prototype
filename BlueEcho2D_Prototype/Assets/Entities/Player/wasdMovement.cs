﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class wasdMovement : MonoBehaviour {
    public float speed = 5f;

    //padding so ship is completely inside viewport's frame
    public float padding = 0.5f;

    private float xMin;
    private float xMax;
    private float yMin;
    private float yMax;

	// Use this for initialization
	void Start () {
        //calculating the viewport's size for auto calculations of Mathf.Clamp
        float distance = transform.position.z - Camera.main.transform.position.z;
        Vector3 leftmost = Camera.main.ViewportToWorldPoint(new Vector3(0, 0, distance));
        Vector3 rightmost = Camera.main.ViewportToWorldPoint(new Vector3(1, 0, distance));
        Vector3 topmost = Camera.main.ViewportToWorldPoint(new Vector3(0, 1, distance));
        Vector3 bottommost = Camera.main.ViewportToWorldPoint(new Vector3(0, 0, distance));
        xMin = leftmost.x + padding;
        xMax = rightmost.x - padding;
        yMin = bottommost.y + padding;
        yMax = 4.0f - padding;//topmost.y - padding;
    }
	
	// Update is called once per frame
	void Update () {
        //transform.Translate(speed * Input.GetAxis("Horizontal") * Time.deltaTime, speed * Input.GetAxis("Vertical") * Time.deltaTime, 0f);

        float movement_x = (speed * Input.GetAxis("Horizontal")) * Time.deltaTime;
        float movement_y = (speed * Input.GetAxis("Vertical")) * Time.deltaTime;

        transform.Translate(movement_x, movement_y, 0f);
        transform.position = new Vector3((Mathf.Clamp(transform.position.x, xMin, xMax)), Mathf.Clamp(transform.position.y, yMin, yMax), transform.position.z);
    }
}
