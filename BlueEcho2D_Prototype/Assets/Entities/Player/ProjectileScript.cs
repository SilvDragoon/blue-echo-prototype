﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ProjectileScript : MonoBehaviour {
    public float damage = 100;
    public float speed = 4.0f;
    //public int levelIndex;

    private string levelNames;
    Vector3 Movement;
    public Vector3 mousePos;
    public float GetDamage()
    {
        return damage;
    }

    public void Hit()
    {
        Destroy(this.gameObject);
    }
	
	private void OnTriggerEnter2D(Collider2D other){
        //Debug.Log("outisde:" + other.gameObject.tag);
        if ((other.gameObject.tag == "BasicEnemy" && other.gameObject.GetComponent<EnemySpriteRenderer>().revealed == true) )
        {
				//Debug.Log("inside:" + other.gameObject.tag);
				Destroy(this.gameObject);
				
				
		}//else if(other.gameObject.tag == "RammerEnemy" && other.gameObject.GetComponent<EnemySpriteRenderer>().revealed == true)
       // {
            //Debug.Log("inside rammer");
        //    Destroy(this.gameObject);
        //}



    }

    // Use this for initialization
    void Start () {
        
        mousePos = Camera.main.ScreenToWorldPoint(Input.mousePosition);


        //test rotation code
        Movement = mousePos - transform.position;
        Movement.z = 0f;
        Movement.Normalize();


        float rot_z = Mathf.Atan2(Movement.y, Movement.x) * Mathf.Rad2Deg;
        transform.rotation = Quaternion.Euler(0f, 0f, rot_z - 90);

    }
	
	// Update is called once per frame
	void Update () {
        

        transform.Translate( new Vector3(0,speed,0) * Time.deltaTime);
        
		
	}
}
