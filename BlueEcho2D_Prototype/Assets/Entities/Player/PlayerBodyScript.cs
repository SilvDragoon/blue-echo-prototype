﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerBodyScript : MonoBehaviour {
	public float EntityHp = 300f;
	private AudioSource playerExplosion;
	public AudioClip pExplosion;
    public Image health;
    public Image health1;
    public Image health2;
    public GameObject pauselevel;
    private bool paused = true;
    // Use this for initialization
    void Start () {
		playerExplosion = GetComponent<AudioSource>();
	}
	
	// Update is called once per frame
	void Update () {
		
		
		
	}
	
	 void OnTriggerEnter2D(Collider2D collider)
    {

       // Debug.Log("Collided with" + collider.gameObject.tag);
        if ((collider.gameObject.tag == "RammerEnemy"))
        {
           // Debug.Log(" rammer");
            EntityHp -= 100;
        }
        if ((collider.gameObject.tag == "EnemyLaser") ){//|| (collider.gameObject.tag == "enemyHeavyLaser")){


            if ((collider.gameObject.tag == "EnemyLaser"))
            {
                EntityHp -= collider.gameObject.GetComponent<EnemyProjectileScript>().damage;
            }
            //Debug.Log(collider.gameObject.tag == "RammerEnemy");
           
           //else if ((collider.gameObject.tag == "enemyHeavyLaser")) {
             //    EntityHp -= collider.gameObject.GetComponent<EnemyHeavyProjectileScript>().damage;

            // }


           
         
		 }

        if (EntityHp == 200f)
        {
            //Debug.Log("health-1");
            health.gameObject.SetActive(false);
        }

        if (EntityHp == 100f)
        {
            //Debug.Log("health-2");
            health.gameObject.SetActive(false);
            health1.gameObject.SetActive(false);
        }
        if (EntityHp <= 0f)
        {
            //Debug.Log("health-3");
            //plays sound as the enemy's location
            AudioSource.PlayClipAtPoint(pExplosion, this.transform.position);
            //Destroy(transform.parent.gameObject);
            transform.parent.gameObject.SetActive(false);
            health.gameObject.SetActive(false);
            health1.gameObject.SetActive(false);
            health2.gameObject.SetActive(false);
            pauselevel.gameObject.SetActive(true);
            Time.timeScale = paused ? 0 : 1;

        }  //Debug.Log("destropyed")
    }
}
