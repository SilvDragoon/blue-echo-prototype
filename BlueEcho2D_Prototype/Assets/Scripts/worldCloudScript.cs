﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class worldCloudScript : MonoBehaviour {
	// value from 0.0 to 1.0
	public static float flareCharge;
    public float meterGrowth = 0.005f;

    public int levelIndex;
    private string[] levelNames;
    private int numLevels = 3;

    public float currentTime = 5;
    public float maxTime = 5;

    public Text fuelText;
    public Image fuelImage;

	// Use this for initialization
	void Start () {
		flareCharge = 0.5f;

        levelNames = new string[numLevels];
        levelNames[0] = "scene1";
        levelNames[1] = "scene2";
        levelNames[2] = "scene3";



    }
	
	// Update is called once per frame
	void Update () {
        GameObject fuelBar = GameObject.FindWithTag("UIFuelBar");
        if (fuelBar)
        {
            fuelBar.GetComponent<Image>().fillAmount = flareCharge;
        }

        //over time charge value upto a maximum of 1.0
        //Debug.Log("flareCharge is:"+flareCharge);
		
		
        if (flareCharge < 1.0)
        {
            flareCharge += meterGrowth;
        }

        /* if (Input.GetMouseButtonDown(1) && flareCharge > 0.0)
         {
             flareCharge = 0f;
         }*/

        if ((!GameObject.FindWithTag("BasicEnemy")) && (!GameObject.FindWithTag("RammerEnemy")))
        {
            if (levelIndex + 1 < numLevels)
            {
                Application.LoadLevel(levelNames[levelIndex+1]);
            }else if(levelIndex + 1 == numLevels)
            {
                Application.LoadLevel(levelNames[0]);
            }
            //Debug.Log("next level");
        }

    }
	
    public void UpdateUI ()
    {
        fuelImage.fillAmount = currentTime / maxTime;
        fuelText.text = "Fuel: " + ((int)(currentTime / maxTime * 100)).ToString();
    }
	// on right click
	
}
/*
	
	minimum flare 0.25
	flare intervals 0.25, 0.5, 0.75, 1.0
	in the flare look at the value flareCharge
	stroe this value
	then set flareCharge in the wolrdCloud to 0.0
	when light spawns it looks at its parent for the value
	grows to appropriate size.
		
*/