﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShootFlare : MonoBehaviour {
    public GameObject flare;
    public Vector3 mouseClickedPos;

    // Use this for initialization
    void Start () {
        mouseClickedPos = transform.position;
	}
	
	// Update is called once per frame
	void Update () {
        if (Input.GetMouseButtonDown(1))
        {
            RaycastHit hit;
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            if (Physics.Raycast(ray, out hit))
            {
                mouseClickedPos = hit.point;
                transform.position = mouseClickedPos;
            }
        }

    }
}
